FROM lscr.io/linuxserver/webtop:debian-icewm

RUN apt update

# Remove useless terminal emulator
RUN apt remove -y stterm && apt autoremove -y

# Install utils
RUN apt install -y --no-install-recommends \
        curl \
        dnsutils \
        git \
        gnome-icon-theme \
        gnome-themes-extra \
        htop \
        iputils-ping \
        lxappearance \
        mc \
        nano \
        neofetch \
        traceroute \
        xz-utils

# Install apps
RUN apt install -y --no-install-recommends \
        filezilla \
        firefox-esr \
        galculator \
        gpicview \
        keepassxc \
        lxterminal \
        mousepad \
        pcmanfm \
        remmina \
        remmina-plugin-rdp \
        remmina-plugin-secret \
        remmina-plugin-vnc \
        xarchiver

# Install VSCode
RUN apt install -y wget gpg && \
    wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg && \
    install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg && \
    echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list && \
    rm -f packages.microsoft.gpg && \
    apt update && apt install -y apt-transport-https code

# Install Moonlight
RUN cd /tmp && \
    wget -O /tmp/moonlight https://github.com/moonlight-stream/moonlight-qt/releases/download/v6.0.0/Moonlight-6.0.0-x86_64.AppImage && \
    chmod +x /tmp/moonlight && \
    /tmp/moonlight --appimage-extract && \
    cp -R /tmp/squashfs-root/usr/* /usr

# # Install Parsec
# RUN wget -O /tmp/libjpeg8.deb https://archive.debian.org/debian/pool/main/libj/libjpeg8/libjpeg8_8b-1_amd64.deb && \
#     wget -O /tmp/parsec-linux.deb https://builds.parsec.app/package/parsec-linux.deb && \
#     apt install -yf /tmp/libjpeg8.deb /tmp/parsec-linux.deb

# Default programs
RUN update-alternatives --set x-terminal-emulator /usr/bin/lxterminal && \
    update-alternatives --set x-www-browser /usr/bin/firefox-esr

# GTK theme
RUN wget -qO- https://github.com/EliverLara/Nordic/releases/download/v2.2.0/Nordic.tar.xz | tar xJf - -C /usr/share/themes/

# Fix for 401 error when using basic auth
RUN sed -i 's|<link rel="manifest" href="manifest.json">|<link rel="manifest" href="manifest.json" crossorigin="use-credentials">|' \
    /kclient/public/index.html

# Nice favicon
RUN curl -o /kclient/public/icon.png https://icons.iconarchive.com/icons/microsoft/fluentui-emoji-3d/512/Raccoon-3d-icon.png && \
    curl -o /kclient/public/favicon.ico https://www.iconarchive.com/download/i137925/microsoft/fluentui-emoji-3d/Raccoon-3d.ico
